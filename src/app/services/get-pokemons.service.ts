import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetPokemonsService {

  constructor(private http: HttpClient) { }

  //use observables instead of promises? Enable stream openers and closers?

  public getPokemon() {
    return this.http.get('https://pokeapi.co/api/v2/pokemon/?limit=50').toPromise();
  }

  public getPokemonByUrl(url:string) {
    return this.http.get(url).toPromise();
  }

  ngOnInit() {
  }
}



