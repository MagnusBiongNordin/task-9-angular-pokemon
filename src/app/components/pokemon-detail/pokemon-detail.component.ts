import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GetPokemonsService } from '../../services/get-pokemons.service';


@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {
  pokeProfil: any = {};
  pokeStat: any = {};
  pokeMoves: any = [];
  pokemon: any;

  constructor(    
    private getPokemonsService: GetPokemonsService,
    private router: Router
    ){ }

  ngOnInit() {  
    this.getPokemonsService.getPokemonByUrl('https://pokeapi.co/api/v2/pokemon/' + this.router.url.split("/")[2])
    .then(resp => {
      this.pokemon = resp
      this.pokeProfil = {
        height: this.pokemon.height,
        weight: this.pokemon.weight,
        abilities: this.pokemon.abilities,
        base_experience: this.pokemon.base_experience
      };
      this.pokeStat = {
        image: this.pokemon.sprites.front_default,
        types: this.pokemon.types,
        stats: this.pokemon.stats,
        name: this.pokemon.name,
        species: this.pokemon.species
      };
      this.pokemon.moves.map(move => {
            this.pokeMoves.push(move.move.name.charAt(0).toUpperCase() + move.move.name.slice(1).replace("-", " "));
      })
    });
  }
}
