import { Component, OnInit } from '@angular/core';
import { GetPokemonsService } from '../../services/get-pokemons.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  constructor(private getPokemonsService: GetPokemonsService) { }

  resp: any = [];
  pokemons: any = [];
  next: string = "";
  pokemon: any = [];


  async ngOnInit() {
    try {
      this.resp = await this.getPokemonsService.getPokemon()
      this.getPokemonNames();
      this.next = this.resp.next;

  } catch (e) {
      console.error(e);
    }
  }

  async getPokemonNames() {
    if(this.resp.results !== undefined || this.resp.results !== null) {
      this.resp.results.forEach(poke => {
        this.getPokemonsService.getPokemonByUrl(poke.url).then(resp => {
         this.pokemon = resp;
         this.pokemons[this.pokemon.id - 1] = this.pokemon;
        });
       });
    }
  }

  async getNextPokemons() {
    await this.getPokemonsService.getPokemonByUrl(this.next).then(resp => {
      this.pokemon = resp;
      this.next = this.pokemon.next;
      this.pokemon.results.forEach(poke => {
        this.getPokemonsService.getPokemonByUrl(poke.url).then(resp => {
          this.pokemon = resp;
          this.pokemons[this.pokemon.id - 1] = resp;
        })
      });
    })
  }
}
