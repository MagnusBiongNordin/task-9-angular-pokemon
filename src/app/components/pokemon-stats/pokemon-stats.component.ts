import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.scss']
})
export class PokemonStatsComponent implements OnInit {

  @Input() pokeInfo:any;

  constructor() { }

  ngOnInit() {
  }

}

/*
  image: resp.sprites.front_default,
  types: resp.types,
  stats: resp.stats,
  name: resp.name,
  species: resp.species
*/