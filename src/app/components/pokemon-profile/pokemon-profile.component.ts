import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent implements OnInit {

  @Input() pokeInfo:Object;

  constructor() { }

  ngOnInit() {
  }

}

/*
  height: resp.height,
  weight: resp.weight,
  abilities: resp.abilities,
  base_experience: resp.base_experience
*/